[Appearance]
ColorScheme=Breeze
DimmValue=47
Font=MesloLGS NF,10,-1,5,50,0,0,0,0,0
UseFontLineChararacters=false

[Cursor Options]
CursorShape=2

[General]
DimWhenInactive=false
Name=Bacterial
Parent=FALLBACK/
TerminalCenter=true

[Interaction Options]
AllowEscapedLinks=false
DropUrlsAsText=false
MiddleClickPasteMode=1
OpenLinksByDirectClickEnabled=true
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true
UnderlineFilesEnabled=true

[Keyboard]
KeyBindings=default

[Scrolling]
HistoryMode=2

[Terminal Features]
BlinkingCursorEnabled=true
VerticalLine=false
